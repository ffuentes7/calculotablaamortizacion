package AIT.microservicios.mappers;

import AIT.core.base.pojos.microservicios.GenericResponse;
import AIT.core.base.pojos.microservicios.Respuesta;
import AIT.microservicios.models.requestAS400.Criterios;
import AIT.microservicios.models.requestAS400.Data;
import AIT.microservicios.models.requestAS400.PlanesPago;
import AIT.microservicios.models.responseAS400.Message;
import AIT.microservicios.models.share.Cust_Info;
import AIT.microservicios.models.share.Header;
import AIT.microservicios.pojos.request.RequestCustom;
import AIT.microservicios.pojos.response.*;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class Mapping {

    @ConfigProperty(name = "mq.channel")
    String channel;
    @ConfigProperty(name = "mq.qmgr")
    String qmgr;
    @ConfigProperty(name = "header.rmt_srvr")
    String rmt_svr;
    @ConfigProperty(name = "mq.nameQueueRS")
    String queueNameRes;
    @ConfigProperty(name = "header.function")
    String appName;
    public ResponseCustom Mapping_AS400_Broker(Message msj) {
        //MAPEO RESPONSE BROKER

        List<PagoEstimado> pagoEstimadoList = new ArrayList<>();

        for (var pagoEstimado:msj.getData().getPagosEstimados().getPagoEstimado()
             ) {

            PagoEstimado pagoEstimado1 = new PagoEstimado(
                    pagoEstimado.getFecha(),
                    pagoEstimado.getTotal(),
                    pagoEstimado.getCapital(),
                    pagoEstimado.getInteres(),
                    pagoEstimado.getSaldoCapital());

            pagoEstimadoList.add(pagoEstimado1);

        }

        PagosEstimados pagosEstimados = new PagosEstimados(pagoEstimadoList);

        InfoGeneral infoGeneral = new InfoGeneral(
                msj.getData().getInfoGeneral().getMontoCapital(),
                msj.getData().getInfoGeneral().getBase(),
                msj.getData().getInfoGeneral().getAnioAcum(),
                msj.getData().getInfoGeneral().getCaptzInt(),
                msj.getData().getInfoGeneral().getTasaInt());

        CalculoTablaAmortizacionResponse calculoTablaAmortizacionResponse = new CalculoTablaAmortizacionResponse(
                infoGeneral,
                pagosEstimados);

        GenericResponse genericResponse = new GenericResponse();

        genericResponse.setIdMsg("OK");
        genericResponse.setDescMsg("La operación fue realizada exitósamente");

        Respuesta respuesta = new Respuesta();
        respuesta.setGenericResponse(genericResponse);

        ResponseCustom responseCustom = new ResponseCustom();
        responseCustom.setCalculoTablaAmortizacionResponse(calculoTablaAmortizacionResponse);
        responseCustom.setRespuesta(respuesta);
        return responseCustom;
    }

    public AIT.microservicios.models.requestAS400.Message Mapping_Broker_AS400(RequestCustom msj)  {


        //MAPEO DEL RQ DE BROKER AL DE AS400
        Criterios criterios = new Criterios(msj.getCalculoTablaAmortizacion().getCriterios().getSaldoCapital(),
                msj.getCalculoTablaAmortizacion().getCriterios().getBaseAcumulacion(),
                msj.getCalculoTablaAmortizacion().getCriterios().getCodIntCaptlzdo(),
                msj.getCalculoTablaAmortizacion().getCriterios().getPosicionesDecimales(),
                msj.getCalculoTablaAmortizacion().getCriterios().getPlazoTotal(),
                msj.getCalculoTablaAmortizacion().getCriterios().getAntoBase(),
                msj.getCalculoTablaAmortizacion().getCriterios().getFechaComienzoInt(),
                msj.getCalculoTablaAmortizacion().getCriterios().getIncluirFeriados());

        PlanesPago planesPago = new PlanesPago(msj.getCalculoTablaAmortizacion().getPlanesPago().getNumeroPlan(),
                msj.getCalculoTablaAmortizacion().getPlanesPago().getPagosPlan(),
                msj.getCalculoTablaAmortizacion().getPlanesPago().getPeriodoPago(),
                msj.getCalculoTablaAmortizacion().getPlanesPago().getFrecuenciaPago(),
                msj.getCalculoTablaAmortizacion().getPlanesPago().getPrimerFechaPago(),
                msj.getCalculoTablaAmortizacion().getPlanesPago().getDiaEspera(),msj.getCalculoTablaAmortizacion().getPlanesPago().getTipoPago(),
                msj.getCalculoTablaAmortizacion().getPlanesPago().getPorcentajePago(),
                msj.getCalculoTablaAmortizacion().getPlanesPago().getTasaInteres());

        Data data = new Data(criterios,planesPago);

        Cust_Info CUST_INFO = new Cust_Info(
                msj.getTipoDocumento(),
                msj.getNroDocumento(),
                "",
                "",
                msj.getHeader().getUsuario());

        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
        Date date = new Date();
        String correlId;
        if(msj.getNroDocumento().length() > 7)
            correlId = (formatter.format(date)+msj.getNroDocumento()).substring(0,24);
        else
            correlId = formatter.format(date)+msj.getNroDocumento();


        Header HEADER = new Header(channel,
                rmt_svr,
                correlId,
                appName,
                queueNameRes,
                qmgr);

        AIT.microservicios.models.requestAS400.Message MESSAGE = new AIT.microservicios.models.requestAS400.Message("RQ",HEADER,CUST_INFO,data);

        return MESSAGE;
    }


}
