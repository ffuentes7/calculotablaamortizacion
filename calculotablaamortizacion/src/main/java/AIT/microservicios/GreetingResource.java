package AIT.microservicios;

import AIT.microservicios.pojos.request.RequestCustom;
import AIT.microservicios.pojos.response.ResponseCustom;
import AIT.microservicios.services.CalculoTablaAmortizacionService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/Siebel/MS/CalculoTablaAmortizacion")
public class GreetingResource {

    @Inject
    CalculoTablaAmortizacionService calculo;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseCustom calcularTablaAmortizacion(RequestCustom msj) {

        return calculo.calcularTablaAmortizacion(msj);
    }

}