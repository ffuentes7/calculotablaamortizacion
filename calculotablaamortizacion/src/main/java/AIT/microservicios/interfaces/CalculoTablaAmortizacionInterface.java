package AIT.microservicios.interfaces;

import AIT.microservicios.pojos.request.RequestCustom;
import AIT.microservicios.pojos.response.ResponseCustom;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface CalculoTablaAmortizacionInterface {
    @WebMethod
    public ResponseCustom calcularTablaAmortizacion(RequestCustom msj);

}
