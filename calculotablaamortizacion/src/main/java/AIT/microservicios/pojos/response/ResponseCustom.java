package AIT.microservicios.pojos.response;

import AIT.core.base.pojos.microservicios.Header;
import AIT.core.base.pojos.microservicios.Response;
import AIT.core.base.pojos.microservicios.Respuesta;
import lombok.Data;

@Data
public class ResponseCustom extends Response {

    private CalculoTablaAmortizacionResponse calculoTablaAmortizacionResponse;

    public ResponseCustom(CalculoTablaAmortizacionResponse calculoTablaAmortizacionResponse) {
        this.calculoTablaAmortizacionResponse = calculoTablaAmortizacionResponse;
    }

    public ResponseCustom() {
    }

    @Override
    public Header getHeader() {
        return super.getHeader();
    }

    @Override
    public void setHeader(Header header) {
        super.setHeader(header);
    }

    @Override
    public Respuesta getRespuesta() {
        return super.getRespuesta();
    }

    @Override
    public void setRespuesta(Respuesta respuesta) {
        super.setRespuesta(respuesta);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
