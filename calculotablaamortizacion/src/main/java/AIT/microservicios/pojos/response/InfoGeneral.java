package AIT.microservicios.pojos.response;

import lombok.Data;

@Data
public class InfoGeneral {

    private String montoCapital;
    private String base;
    private String anioAcum;
    private String captzInt;
    private String tasaInt;

    public InfoGeneral() {
    }

    public InfoGeneral(String montoCapital, String base, String anioAcum, String captzInt, String tasaInt) {
        this.montoCapital = montoCapital;
        this.base = base;
        this.anioAcum = anioAcum;
        this.captzInt = captzInt;
        this.tasaInt = tasaInt;
    }
}
