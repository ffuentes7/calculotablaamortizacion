package AIT.microservicios.pojos.response;

import lombok.Data;

import java.util.List;
@Data
public class PagosEstimados {

    private List<PagoEstimado> pagoEstimado;

    public PagosEstimados(List<PagoEstimado> pagoEstimado) {
        this.pagoEstimado = pagoEstimado;
    }

    public PagosEstimados() {
    }

}
