package AIT.microservicios.pojos.response;

import lombok.Data;

@Data
public class PagoEstimado {

    private String fecha;
    private String total;
    private String Capital;
    private String interes;
    private String saldoCapital;

    public PagoEstimado(String fecha, String total, String capital, String interes, String saldoCapital) {
        this.fecha = fecha;
        this.total = total;
        Capital = capital;
        this.interes = interes;
        this.saldoCapital = saldoCapital;
    }

    public PagoEstimado() {
    }
}
