package AIT.microservicios.pojos.response;

import lombok.Data;

@Data
public class CalculoTablaAmortizacionResponse {

    private InfoGeneral infoGeneral;
    private PagosEstimados pagosEstimados;

    public CalculoTablaAmortizacionResponse() {
    }

    public CalculoTablaAmortizacionResponse(InfoGeneral infoGeneral, PagosEstimados pagosEstimados) {
        this.infoGeneral = infoGeneral;
        this.pagosEstimados = pagosEstimados;
    }
}
