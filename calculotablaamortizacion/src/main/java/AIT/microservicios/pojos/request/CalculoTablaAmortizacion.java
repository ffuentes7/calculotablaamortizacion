package AIT.microservicios.pojos.request;

import lombok.Data;

@Data
public class CalculoTablaAmortizacion {

    private Criterios criterios;
    private PlanesPago planesPago;

    public CalculoTablaAmortizacion() {
    }

    public CalculoTablaAmortizacion(Criterios criterios, PlanesPago planesPago) {
        this.criterios = criterios;
        this.planesPago = planesPago;
    }
}
