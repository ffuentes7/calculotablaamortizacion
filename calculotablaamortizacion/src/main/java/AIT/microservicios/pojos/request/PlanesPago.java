package AIT.microservicios.pojos.request;

import lombok.Data;

@Data
public class PlanesPago {

    private String numeroPlan;
    private String pagosPlan;
    private String periodoPago;
    private String frecuenciaPago;
    private String primerFechaPago;
    private String diaEspera;
    private String tipoPago;
    private String porcentajePago;
    private String tasaInteres;

    public PlanesPago() {
    }

    public PlanesPago(String numeroPlan, String pagosPlan, String periodoPago, String frecuenciaPago, String primerFechaPago, String diaEspera, String tipoPago, String porcentajePago, String tasaInteres) {
        this.numeroPlan = numeroPlan;
        this.pagosPlan = pagosPlan;
        this.periodoPago = periodoPago;
        this.frecuenciaPago = frecuenciaPago;
        this.primerFechaPago = primerFechaPago;
        this.diaEspera = diaEspera;
        this.tipoPago = tipoPago;
        this.porcentajePago = porcentajePago;
        this.tasaInteres = tasaInteres;
    }
}
