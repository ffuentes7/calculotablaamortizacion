package AIT.microservicios.pojos.request;

import lombok.Data;

@Data
public class Criterios {

    private String saldoCapital;
    private String baseAcumulacion;
    private String codIntCaptlzdo;
    private String posicionesDecimales;
    private String plazoTotal;
    private String antoBase;
    private String fechaComienzoInt;
    private String incluirFeriados;

    public Criterios() {
    }

    public Criterios(String saldoCapital, String baseAcumulacion, String codIntCaptlzdo, String posicionesDecimales, String plazoTotal, String antoBase, String fechaComienzoInt, String incluirFeriados) {
        this.saldoCapital = saldoCapital;
        this.baseAcumulacion = baseAcumulacion;
        this.codIntCaptlzdo = codIntCaptlzdo;
        this.posicionesDecimales = posicionesDecimales;
        this.plazoTotal = plazoTotal;
        this.antoBase = antoBase;
        this.fechaComienzoInt = fechaComienzoInt;
        this.incluirFeriados = incluirFeriados;
    }
}
