package AIT.microservicios.pojos.request;

import AIT.core.base.pojos.microservicios.Header;
import AIT.core.base.pojos.microservicios.Request;
import lombok.Data;

@Data
public class RequestCustom extends Request {

    private CalculoTablaAmortizacion calculoTablaAmortizacion;

    public RequestCustom() {
    }

    public RequestCustom(CalculoTablaAmortizacion calculoTablaAmortizacion) {
        this.calculoTablaAmortizacion = calculoTablaAmortizacion;
    }

    @Override
    public Header getHeader() {
        return super.getHeader();
    }

    @Override
    public void setHeader(Header header) {
        super.setHeader(header);
    }

    @Override
    public String getTipoDocumento() {
        return super.getTipoDocumento();
    }

    @Override
    public void setTipoDocumento(String tipoDocumento) {
        super.setTipoDocumento(tipoDocumento);
    }

    @Override
    public String getNroDocumento() {
        return super.getNroDocumento();
    }

    @Override
    public void setNroDocumento(String nroDocumento) {
        super.setNroDocumento(nroDocumento);
    }

    @Override
    public String getNroSolicitud() {
        return super.getNroSolicitud();
    }

    @Override
    public void setNroSolicitud(String nroSolicitud) {
        super.setNroSolicitud(nroSolicitud);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
