package AIT.microservicios.models.requestAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CRITERIOS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Criterios {
    @XmlElement(name = "SALDOCAPITAL")
    private String saldoCapital;
    @XmlElement(name = "BASEACUMULACION")
    private String baseAcumulacion;
    @XmlElement(name = "CODINTCAPTLZDO")
    private String codIntCaptlzdo;
    @XmlElement(name = "POSICIONESDECIMALES")
    private String posicionesDecimales;
    @XmlElement(name = "PLAZOTOTAL")
    private String plazoTotal;
    @XmlElement(name = "ANTOBASE")
    private String antoBase;
    @XmlElement(name = "FECHACOMIENZOINT")
    private String fechaComienzoInt;
    @XmlElement(name = "INCLUIRFERIADOS")
    private String incluirFeriados;

    public Criterios() {
    }

    public Criterios(String saldoCapital, String baseAcumulacion, String codIntCaptlzdo, String posicionesDecimales, String plazoTotal, String antoBase, String fechaComienzoInt, String incluirFeriados) {
        this.saldoCapital = saldoCapital;
        this.baseAcumulacion = baseAcumulacion;
        this.codIntCaptlzdo = codIntCaptlzdo;
        this.posicionesDecimales = posicionesDecimales;
        this.plazoTotal = plazoTotal;
        this.antoBase = antoBase;
        this.fechaComienzoInt = fechaComienzoInt;
        this.incluirFeriados = incluirFeriados;
    }
}
