package AIT.microservicios.models.requestAS400;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@lombok.Data
@XmlRootElement(name = "DATA")
@XmlAccessorType(XmlAccessType.FIELD)
public class Data {
    @XmlElement(name = "CRITERIOS")
    private Criterios criterios;
    @XmlElement(name = "PLANESPAGO")
    private PlanesPago planesPago;

    public Data() {
    }

    public Data(Criterios criterios, PlanesPago planesPago) {
        this.criterios = criterios;
        this.planesPago = planesPago;
    }
}
