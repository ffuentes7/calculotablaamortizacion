package AIT.microservicios.models.requestAS400;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PLANESPAGO")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlanesPago {

    @XmlElement(name = "NUMEROPLAN")
    private String numeroPlan;
    @XmlElement(name = "PAGOSPLAN")
    private String pagosPlan;
    @XmlElement(name = "PERIODOPAGO")
    private String periodoPago;
    @XmlElement(name = "FRECUENCIAPAGO")
    private String frecuenciaPago;
    @XmlElement(name = "PRIMERFECHAPAGO")
    private String primerFechaPago;
    @XmlElement(name = "DIAESPERA")
    private String diaEspera;
    @XmlElement(name = "TIPOPAGO")
    private String tipoPago;
    @XmlElement(name = "PORCENTAJEPAGO")
    private String porcentajePago;
    @XmlElement(name = "TASAINTERES")
    private String tasaInteres;

    public PlanesPago() {
    }

    public PlanesPago(String numeroPlan, String pagosPlan, String periodoPago, String frecuenciaPago, String primerFechaPago, String diaEspera, String tipoPago, String porcentajePago, String tasaInteres) {
        this.numeroPlan = numeroPlan;
        this.pagosPlan = pagosPlan;
        this.periodoPago = periodoPago;
        this.frecuenciaPago = frecuenciaPago;
        this.primerFechaPago = primerFechaPago;
        this.diaEspera = diaEspera;
        this.tipoPago = tipoPago;
        this.porcentajePago = porcentajePago;
        this.tasaInteres = tasaInteres;
    }
}
