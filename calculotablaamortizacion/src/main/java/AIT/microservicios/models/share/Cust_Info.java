package AIT.microservicios.models.share;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@Data
@XmlRootElement(name = "CUST_INFO")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cust_Info {

    @XmlElement(name = "DOC_TYPE")
    public String doc_type;
    @XmlElement(name = "DOC_NUMBER")
    public String doc_number;
    @XmlElement(name = "CHILD_DOC_TYPE")
    public String child_doc_type;
    @XmlElement(name = "CHILD_DOC_NUMBER")
    public String child_doc_number;
    @XmlElement(name = "USERID")
    public String userId;

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }

    public String getDoc_number() {
        return doc_number;
    }

    public void setDoc_number(String doc_number) {
        this.doc_number = doc_number;
    }

    public String getChild_doc_type() {
        return child_doc_type;
    }

    public void setChild_doc_type(String child_doc_type) {
        this.child_doc_type = child_doc_type;
    }

    public String getChild_doc_number() {
        return child_doc_number;
    }

    public void setChild_doc_number(String child_doc_number) {
        this.child_doc_number = child_doc_number;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Cust_Info() {
    }

    public Cust_Info(String doc_type, String doc_number, String child_doc_type, String child_doc_number, String userId) {
        this.doc_type = doc_type;
        this.doc_number = doc_number;
        this.child_doc_type = child_doc_type;
        this.child_doc_number = child_doc_number;
        this.userId = userId;
    }
}
