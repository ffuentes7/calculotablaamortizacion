package AIT.microservicios.models.responseAS400;

import javax.xml.bind.annotation.*;

@lombok.Data
@XmlRootElement(name = "DATA")
@XmlAccessorType(XmlAccessType.FIELD)
public class Data {
    @XmlAttribute(name = "RESULT")
    private String resultado;
    @XmlElement(name = "INFOGENERAL")
    private InfoGeneral infoGeneral;
    @XmlElement(name = "PAGOSESTIMADOS")
    private PagosEstimados pagosEstimados;

    public Data() {
    }

    public Data(String resultado, InfoGeneral infoGeneral, PagosEstimados pagosEstimados) {
        this.resultado = resultado;
        this.infoGeneral = infoGeneral;
        this.pagosEstimados = pagosEstimados;
    }
}
