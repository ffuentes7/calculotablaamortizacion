package AIT.microservicios.models.responseAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
@Data
@XmlRootElement(name = "PAGOSESTIMADOS")
@XmlAccessorType(XmlAccessType.FIELD)
public class PagosEstimados {

    @XmlElement(name = "PAGOESTIMADO")
    private List<PagoEstimado> pagoEstimado;

    public PagosEstimados() {
    }

    public PagosEstimados(List<PagoEstimado> pagoEstimado) {
        this.pagoEstimado = pagoEstimado;
    }
}
