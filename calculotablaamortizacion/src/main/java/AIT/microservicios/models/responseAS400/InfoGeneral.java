package AIT.microservicios.models.responseAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "INFOGENERAL")
@XmlAccessorType(XmlAccessType.FIELD)
public class InfoGeneral {
    @XmlElement(name = "MONTOCAPITAL")
    private String montoCapital;
    @XmlElement(name = "BASE")
    private String base;
    @XmlElement(name = "ANIOACUM")
    private String anioAcum;
    @XmlElement(name = "CAPTZINT")
    private String captzInt;
    @XmlElement(name = "TASAINT")
    private String tasaInt;

    public InfoGeneral() {
    }

    public InfoGeneral(String montoCapital, String base, String anioAcum, String captzInt, String tasaInt) {
        this.montoCapital = montoCapital;
        this.base = base;
        this.anioAcum = anioAcum;
        this.captzInt = captzInt;
        this.tasaInt = tasaInt;
    }
}
