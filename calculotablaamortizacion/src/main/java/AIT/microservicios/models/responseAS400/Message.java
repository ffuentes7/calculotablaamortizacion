package AIT.microservicios.models.responseAS400;

import AIT.microservicios.models.share.Cust_Info;
import AIT.microservicios.models.share.Header;

import javax.xml.bind.annotation.*;

@lombok.Data
@XmlRootElement(name = "MESSAGE")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {

    @XmlAttribute(name = "TYPE")
    private String type;
    @XmlElement(name = "HEADER")
    private Header header;
    @XmlElement(name = "SECURITY")
    private Security security;
    @XmlElement(name = "CUST_INFO")
    private Cust_Info custInfo;
    @XmlElement(name = "DATA")
    private Data data;

    public Message(String type, Header header, Security security, Cust_Info custInfo, Data data) {
        this.type = type;
        this.header = header;
        this.security = security;
        this.custInfo = custInfo;
        this.data = data;
    }

    public Message() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }

    public Cust_Info getCustInfo() {
        return custInfo;
    }

    public void setCustInfo(Cust_Info custInfo) {
        this.custInfo = custInfo;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
