package AIT.microservicios.models.responseAS400;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "PAGOESTIMADO")
@XmlAccessorType(XmlAccessType.FIELD)
public class PagoEstimado {
    @XmlElement(name = "FECHA")
    private String fecha;
    @XmlElement(name = "TOTAL")
    private String total;
    @XmlElement(name = "CAPITAL")
    private String Capital;
    @XmlElement(name = "INTERES")
    private String interes;
    @XmlElement(name = "SALDOCAPITAL")
    private String saldoCapital;

    public PagoEstimado() {
    }

    public PagoEstimado(String fecha, String total, String capital, String interes, String saldoCapital) {
        this.fecha = fecha;
        this.total = total;
        Capital = capital;
        this.interes = interes;
        this.saldoCapital = saldoCapital;
    }
}
