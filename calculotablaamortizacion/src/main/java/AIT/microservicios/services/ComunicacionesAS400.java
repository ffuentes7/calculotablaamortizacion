package AIT.microservicios.services;

import AIT.core.base.pojos.microservicios.ErrorGeneral;
import AIT.core.base.pojos.microservicios.Respuesta;
import AIT.microservicios.mappers.Mapping;
import AIT.microservicios.models.responseAS400.Message;
import AIT.microservicios.models.tools.Objeto;
import AIT.microservicios.pojos.request.RequestCustom;
import AIT.microservicios.pojos.response.ResponseCustom;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

@ApplicationScoped
public class ComunicacionesAS400 {

    @Inject
    Mapping map;

    public ResponseCustom ConvertToJavaObject(String response) {

        try {

            JAXBContext context = JAXBContext.newInstance(Message.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Message msj = (Message) unmarshaller.unmarshal(new StringReader(response));

            if (msj.getData().getResultado().equals("TRUE")){

                ResponseCustom responseBroker = map.Mapping_AS400_Broker(msj);

                return responseBroker;

            }else{

                String errorCode = msj.getHeader().getErrorCode();
                String msgError = msj.getHeader().getErrorDescription();
                String descError = "Ha ocurrido un error en el AS400, favor verificar el codigo de error" +
                        " y su mensaje. Contacte al equipo del core o verifique los logs de la Funcion en IFC";

                ResponseCustom responseBroker = generateError(errorCode, msgError, descError);

                return responseBroker;

            }


        } catch (JAXBException e){

            e.printStackTrace();
            System.err.println("Error: "+e.getErrorCode().toString());
            System.err.println("Descripcion: "+e.getMessage().toString());

            String descError = "Ha ocurrido un error durante el proceso Unmarshall del Java," +
                    " favor verificar el codigo de error y su mensaje. Asegurese de que el parseo que se este realizando " +
                    "sea el correcto";

            ResponseCustom responseBroker = generateError(e.getErrorCode().toString(),e.getMessage().toString(),descError);

            return responseBroker;

        }

    }

    public ResponseCustom generateError(String errorCode, String msgError, String descError) {

        ResponseCustom responseBroker = new ResponseCustom();

        Respuesta respuesta = new Respuesta();

        ErrorGeneral errorGeneral = new ErrorGeneral();

        errorGeneral.setIdError(errorCode);
        errorGeneral.setMsgError(msgError);
        errorGeneral.setDescError(descError);

        respuesta.setErrorGeneral(errorGeneral);
        responseBroker.setRespuesta(respuesta);
        return responseBroker;
    }

    public Objeto ConvertToXml(RequestCustom msj) {

        AIT.microservicios.models.requestAS400.Message MESSAGE = map.Mapping_Broker_AS400(msj);

        try {

            JAXBContext context = JAXBContext.newInstance(AIT.microservicios.models.requestAS400.Message.class);
            Marshaller marshaller = context.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);

            StringWriter writer = new StringWriter();

            marshaller.marshal(MESSAGE,writer);

            String requestAs400 = writer.toString();
            QuitarSaltos(requestAs400);

            Objeto obj = new Objeto();

            obj.setCorrelID(MESSAGE.getHeader().getCorrelationId());
            obj.setXml(requestAs400);
            obj.setError(null);

            return obj;

        } catch (JAXBException e ) {
            e.printStackTrace();
            System.out.println("Error:"+e.getErrorCode().toString());
            System.out.println("Descripcion:"+e.getMessage().toString());

            String error = "error|"+e.getErrorCode().toString()+"|"+e.getMessage().toString();

            Objeto obj = new Objeto();

            obj.setError(error);

            return obj;

        }

    }

    public String QuitarSaltos(String cadena) {
        // Elimina espacios, tabuladores, retornos
        cadena.replaceAll(" ","");
        cadena.replaceAll("\t","");
        cadena.replaceAll("\n", "");
        return cadena;
    }


}
