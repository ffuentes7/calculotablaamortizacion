package AIT.microservicios.services;

import AIT.core.base.logs.LogFile;
import AIT.core.base.pojos.microservicios.ConsumoMq;
import AIT.core.base.services.microservicios.IbmMqService;
import AIT.core.base.utils.BaseUtils;
import AIT.microservicios.interfaces.CalculoTablaAmortizacionInterface;
import AIT.microservicios.models.tools.Objeto;
import AIT.microservicios.pojos.request.RequestCustom;
import AIT.microservicios.pojos.response.ResponseCustom;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Optional;

@ApplicationScoped
@WebService(endpointInterface = "AIT.microservicios.interfaces.CalculoTablaAmortizacionInterface")
public class CalculoTablaAmortizacionService implements CalculoTablaAmortizacionInterface {

    @Inject
    ComunicacionesAS400 consultarAS400;
    IbmMqService mqService = new IbmMqService();
    @ConfigProperty(name = "mq.host")
    public String host;
    @ConfigProperty(name = "mq.port")
    public int port;
    @ConfigProperty(name = "mq.channel")
    public String channel;
    @ConfigProperty(name = "mq.qmgr")
    public String qmgr;
    @ConfigProperty(name = "mq.appUser")
    public String appUser;
    @ConfigProperty(name = "mq.appPassword")
    public Optional<String> appPassword;
    @ConfigProperty(name = "mq.nameQueueRQ")
    public String queueNameReq;
    @ConfigProperty(name = "mq.nameQueueRS")
    public String queueNameRes;
    @ConfigProperty(name = "header.function")
    public String appName;

    @Override
    public ResponseCustom calcularTablaAmortizacion(RequestCustom msj)  {

        if (msj.getNroDocumento() == null || msj.getTipoDocumento() == null) {

            String errorCode = "ERR0001";
            String msgError = "Es necesario un número de documento para poder realizar la consulta";
            String descError = "Ha ocurrido un error mientras se recibía la petición. " +
                    "Asegúrese que el número de Documento o tipo de Documento se envie en la petición";


            ResponseCustom responseBroker = consultarAS400.generateError(errorCode, msgError, descError);

            responseBroker.setHeader(msj.getHeader());

            return responseBroker;

        }

        //Se envía el REQUEST para Parsear al REQUEST del AS400
        Objeto xml = consultarAS400.ConvertToXml(msj);

        if (xml.getError() != null) {

            String errorCode = xml.getError().split("|")[1];
            String msgError = xml.getError().split("|")[2];
            String descError = "Ha ocurrido un error durante el proceso Marshall del Java," +
                    " favor verificar el codigo de error y su mensaje. Asegurese de que el parseo que se este realizando " +
                    "sea el correcto";


            ResponseCustom responseBroker = consultarAS400.generateError(errorCode, msgError, descError);

            responseBroker.setHeader(msj.getHeader());

            return responseBroker;

        }

        ConsumoMq mqParameter = new ConsumoMq.ConsumoMqBuilder(
                host,
                port,
                channel,
                qmgr,
                appUser,
                appPassword.orElse(""),
                queueNameReq).
                setApplicationName(appName).setCorrelId(xml.getCorrelID()).setRequiredAutentication(Boolean.FALSE).build();

        ConsumoMq mqParameterRS = new ConsumoMq.ConsumoMqBuilder(
                host,
                port,
                channel,
                qmgr,
                appUser,
                appPassword.orElse(""),
                queueNameRes).
                setApplicationName(appName).setCorrelId(BaseUtils.getCorrelIdHexa(xml.getCorrelID())).setRequiredAutentication(Boolean.FALSE).build();

        String responseAS400;

        //Se envía y se recibe por MQ al AS400
        try {

            responseAS400 = mqService.consumirIbmEnvioRecibirMq(mqParameter, mqParameterRS, xml.getXml(), new LogFile());

        } catch (Exception e) {

            String errorCode = "ERR0002";
            String msgError = "No se ha recibido respuesta del AS400";
            String descError = "Ha ocurrido un error durante el proceso get en la cola "+queueNameRes+
                    ", favor verificar los parametros de Conexion con la Cola. Asegurese que la funcion de IFC " +
                    "se encuentre activa";

            ResponseCustom responseBroker = consultarAS400.generateError(errorCode, msgError, descError);

            responseBroker.setHeader(msj.getHeader());
            return responseBroker;

        }

        try{

            //Se envia el RESPONSE del AS400 para Parsear al RESPONSE
            ResponseCustom responseBroker = consultarAS400.ConvertToJavaObject(responseAS400);
            responseBroker.setHeader(msj.getHeader());

            return responseBroker;

        }catch (Exception e){

            e.printStackTrace();
            System.err.println("Error: ERR003");
            System.err.println("Descripcion: "+e.getMessage().toString());

            String descError = "Ha ocurrido un error durante el mapeo entre el Response del Core y el Response de Broker," +
                    " favor verificar el mensaje de respuesta que se esta enviando. Asegurese de que el parseo que se este realizando " +
                    "sea el correcto o que este llegando la información completa." +
                    "" +
                    "Mensaje de Respuesta: "+ responseAS400;

            ResponseCustom responseBroker = consultarAS400.generateError("ERR003",e.getMessage().toString(),descError);
            responseBroker.setHeader(msj.getHeader());

            return responseBroker;

        }


    }

}
